<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFotoObservacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('foto_observaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('foto_url');
            $table->integer('observacion_id')->unsigned()->nullable();
            $table->foreign('observacion_id')->references('id')
            ->on('observaciones')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('foto_observaciones');
    }
}
