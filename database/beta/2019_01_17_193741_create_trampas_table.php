<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrampasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trampas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('feromonas');
            $table->integer('dispositivo_id')->unsigned()->nullable();
            $table->foreign('dispositivo_id')->references('id')
            ->on('dispositivos')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->integer('producto_id')->unsigned()->nullable();
            $table->foreign('producto_id')->references('id')
            ->on('productos')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->integer('orden_id')->unsigned()->nullable();
            $table->foreign('orden_id')->references('id')
            ->on('ordenes')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->integer('numero_estacion');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trampas');
    }
}
