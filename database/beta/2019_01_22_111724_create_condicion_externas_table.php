<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCondicionExternasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('condiciones_externas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('maquinaria_obsoleta')->nullable();
            $table->string('drenaje')->nullable();
            $table->string('escombros')->nullable();
            $table->string('contenedores')->nullable();
            $table->string('focos')->nullable();
            $table->string('areas_verdes')->nullable();
            $table->string('residuos')->nullable();
            $table->string('anidacion')->nullable();
            $table->integer('orden_id')->unsigned()->nullable();
            $table->foreign('orden_id')->references('id')
            ->on('ordenes')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('condiciones_externas');
    }
}
