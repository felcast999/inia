<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLamparasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lamparas', function (Blueprint $table) {
            $table->increments('id');
            $table->text('estado')->nullable();
            $table->integer('numero_estacion');
            $table->string('cambio_plancha')->nullable();
            $table->text('observacion_funcionamiento')->nullable();
            $table->string('limpieza_de_lampara')->nullable();
            $table->integer('cambio_de_fluorecente')->nullable();
            $table->integer('orden_id')->unsigned()->nullable();
            $table->foreign('orden_id')->references('id')
            ->on('ordenes')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lamparas');
    }
}
