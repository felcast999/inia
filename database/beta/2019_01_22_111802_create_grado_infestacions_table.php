<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGradoInfestacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grado_infestaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cucaracha_alemana')->nullable();
            $table->string('moscas')->nullable();
            $table->string('abejas')->nullable();
            $table->string('termitas')->nullable();
            $table->string('pulgas')->nullable();
            $table->string('polilla')->nullable();
            $table->string('gorgojos')->nullable();
            $table->string('cucaracha_americana')->nullable();

            $table->string('hormigas')->nullable();
            $table->string('comejen')->nullable();
            $table->string('aranas')->nullable();
            $table->string('roedores')->nullable();
            $table->string('basureros')->nullable();
            $table->string('zancudos')->nullable();
            $table->string('otros')->nullable();
            $table->integer('orden_id')->unsigned()->nullable();
            $table->foreign('orden_id')->references('id')
            ->on('ordenes')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grado_infestaciones');
    }
}
