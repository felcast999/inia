<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlagaLamparasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plaga_lamparas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('plaga_id')->unsigned()->nullable();
            $table->foreign('plaga_id')->references('id')
            ->on('plagas')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->integer('cantidad')->default(0);
            $table->integer('lampara_id')->unsigned()->nullable();
            $table->foreign('lampara_id')->references('id')
            ->on('lamparas')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plaga_lamparas');
    }
}
