<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFumigacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fumigaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('aplicacion_id')->unsigned()->nullable();
            $table->foreign('aplicacion_id')->references('id')
            ->on('aplicaciones')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->integer('producto_id')->unsigned()->nullable();
            $table->foreign('producto_id')->references('id')
            ->on('productos')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->integer('lugar_aplicacion_id')->unsigned()->nullable();
            $table->foreign('lugar_aplicacion_id')->references('id')
            ->on('lugar_aplicaciones')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->string('dosis')->nullable();
            $table->integer('orden_id')->unsigned()->nullable();
            $table->foreign('orden_id')->references('id')
            ->on('ordenes')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fumigaciones');
    }
}
