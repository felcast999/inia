<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCondicionInternasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('condiciones_internas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('paredes')->nullable();
            $table->string('pisos')->nullable();
            $table->string('limpieza')->nullable();
            $table->string('ventanas')->nullable();
            $table->string('almacenamiento')->nullable();
            $table->string('espacio')->nullable();
            $table->string('evidencias')->nullable();
            $table->string('estructuras')->nullable();

            $table->string('techos')->nullable();
            $table->string('maquinarias')->nullable();
            $table->string('puertas')->nullable();
            $table->string('plagas_instalaciones')->nullable();
            $table->string('basureros')->nullable();
            $table->string('equipos')->nullable();
            $table->string('ventilacion')->nullable();
            $table->string('ductos')->nullable();
            $table->integer('orden_id')->unsigned()->nullable();
            $table->foreign('orden_id')->references('id')
            ->on('ordenes')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('condiciones_internas');
    }
}
