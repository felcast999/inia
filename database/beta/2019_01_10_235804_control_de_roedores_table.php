<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ControlDeRoedoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('control_de_roedores', function (Blueprint $table) {
            $table->increments('id');
            $table->text('ubicacion')->nullable();
            $table->string('limpieza_aceptable')->nullable();
            $table->string('limpieza_realizada')->nullable();
            $table->string('estado_cebo')->nullable();
            $table->string('actividad')->nullable();
            $table->string('accion_correctiva')->nullable();
            $table->integer('dispositivo_id')->unsigned()->nullable();
            $table->foreign('dispositivo_id')->references('id')
            ->on('dispositivos')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            $table->integer('producto_id')->unsigned()->nullable();
            $table->foreign('producto_id')->references('id')
            ->on('productos')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            $table->integer('orden_id')->unsigned()->nullable();
            $table->foreign('orden_id')->references('id')
            ->on('ordenes')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->integer('numero_dispositivo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('control_de_roedores');
    }
}
