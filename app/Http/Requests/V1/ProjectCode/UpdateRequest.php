<?php

namespace App\Http\Requests\V1\ProjectCode;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Rules\CheckAttributesInDeleteRecordsRule;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;


class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */



        public function rules()
        {
            return [
                'actividades_idi_id' => 'required',
                'dependencia_id' => 'required',
                'sede_id' => 'required',
                'numero_orden'=>'required',
                'ano'=>'required',
                'tiempo_creacion'=>'required'
    
           
            ];
        }
    
    
    
    public function attributes()
    {
        return [
            'actividades_idi_id' => 'Actividades IDI',
            'dependencia_id' => 'Dependencia',
            'sede_id' => 'Sede',
            'numero_orden' => 'Numero de Orden',
            'ano' => 'Año',
            'tiempo_creacion' => 'Tiempo de creacion'

        ];
    }
 

    


    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(respondWithJson(false,422,$validator->errors()->get('*')));
    }
}
