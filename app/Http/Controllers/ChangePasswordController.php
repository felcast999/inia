<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ChangePassword\ChangePasswordRequest;
use App\Models\User;
use DB;
use App\Exports\ProductsExport;
use Maatwebsite\Excel\Facades\Excel;


class ChangePasswordController extends Controller
{

       public function __construct()
    {
       #$this->middleware('role:super_admin,admin,administration_manager,system_user');
    }
    public function process(ChangePasswordRequest $request)
    {

        $this->changePassword($request);
        $this->removePasswordResetRegister($request);



        return respondWithXML(true,200,'Contrasena actualizada!, Inicie sesion con nueva contrasena');

    
    }

    public function changePassword($request)
    {
        $user = User::whereEmail($request->email)->first();
        $user->update(['password' => $request->password]);

    }

    public function removePasswordResetRegister($request)
    {
        DB::table('password_resets')->where([
            'email' => $request->email,
            'token' => $request->resetToken
        ])->delete();

    }


    public function export() 
    {
        return Excel::download(new ProductsExport, 'xbox.xlsx');
    }
}
