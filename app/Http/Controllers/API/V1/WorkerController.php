<?php
namespace App\Http\Controllers\API\V1;
use App\Http\Controllers\Controller;

use App\Datatables\Worker\WorkerDataTable;

use Illuminate\Http\Request;
use App\Models\Worker;


use App\Http\Requests\V1\Worker\CreateRequest;
use App\Http\Requests\V1\Worker\UpdateRequest;

use App\UseCases\Worker\Create;
use App\UseCases\Worker\Update;
use App\UseCases\Worker\Show;
use App\UseCases\Worker\Destroy;

class WorkerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataTable(WorkerDataTable $table)
    {

        return $table->build();
    }

    public function index()
    {
        //
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(
        CreateRequest $request,
        Create $create
    )
    {

         $create->execute($request);

        return response()->json([
            'message' => 'Empleado registrado!'
        ]);
    }
   

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(
        UpdateRequest $request,
        Update $update
    )
    {
        $update->execute($request);

        return response()->json([
            'message' => 'Empleado modificado!'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */ 
    public function destroy(
        Destroy $destroy,
         $id
    )
    {

        $destroy->execute($id);

        return response()->json([
            'message' => "Empleado eliminado!"
        ]);
    }
     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   
    public function show(
        $id,
        Show $show
    )
    {
        $model = $show->execute($id);

        return response()->json($model);
    }

   
 
}
