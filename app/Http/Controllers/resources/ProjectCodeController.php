<?php

namespace App\Http\Controllers\resources;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{CodigoProyecto,ActividadIdi,Dependencia,Sede};

class ProjectCodeController extends Controller
{
    


    public function get()
    {
        $actividades_idi_ids=ActividadIdi::all();
        $dependencia_ids=Dependencia::all();
        $sede_ids=Sede::all();
        $order_number=str_pad((CodigoProyecto::get()->count()+1), 4, '0', STR_PAD_LEFT);

        return response()->json(compact('actividades_idi_ids','dependencia_ids','sede_ids','order_number'),200);
    }
}
