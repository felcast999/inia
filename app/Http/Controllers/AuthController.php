<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Requests\Auth\LoginRequest;


use App\Models\User;
use App\Models\Role;
use App\Events\LoginHistory;

use DB;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */

    private $user;

    public function __construct(User $user)
    {
        $this->user=$user;
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request)
    {



    	$credentials = request(['email', 'password']);

    	try {
    		if (!auth()->attempt($credentials)) {
    			return response()->json(['error' => __('auth.invalid')], 401);
    		}
    	} catch (\Exception $e) {
            return respondWithJson(false,500,$e->getMessage());

        
        }

        $user=User::where('email', request(['email']))->firstOrFail();

        $tokenResult = $user->createToken('Personal Access Token');

        event(new LoginHistory($user));
        

        

        return $this->respondWithToken($tokenResult->accessToken,$user);
    }

    /**
     * registrar nuevo usuario.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(RegisterRequest $request)
    {
    	$user = User::create([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
    		'email' => $request->get('email'),
    		'password' => $request->get('password'),
            'phone'=>$request->get('phone'),
            'document_number'=>$request->get('document_number'),
            'status_id'=>1
    	]);


        $admin_role = Role::where('name','admin')->first();
        $super_admin_role = Role::where('name','super_admin')->first();


        $user->roles()->attach([$admin_role->id, $super_admin_role->id]);

        $credentials = request(['email', 'password']);
       
        $tokenResult = $user->createToken('Personal Access Token');

        $token = $tokenResult->token;

        return $this->respondWithToken($tokenResult->accessToken,$user);

    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
  

    public function me()
    {
        $auth_user = auth()->user();
        
        $auth_user->withRelationships();


    	return response()->json($auth_user);
    }

    
    public function logout()
    {


        try {

            auth()->user()->token()->revoke();

            return respondWithXML(true,200,__('session.logout'));


        } catch(\Exception $e)
        {

            return respondWithJson(false,500,$e->getMessage());

           
        }

    }

 

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token,$user=null)
    {

        
         $user->load('roles');
         $data=[
            'access_token' => $token,
            'token_type' => 'bearer',
            'user' => json_decode($user,true)
        ];

        return respondWithJson(true,200,"Exito!",$data);
    	
    }
    
}
