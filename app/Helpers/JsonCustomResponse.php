<?php

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\JsonResource;

if (! function_exists('respondWithJson')) {
    function respondWithJson($success,$status_code=200, $message=[],$data=[])
	{
		return response()->json(
            [
                'success' => $success,
                'status'=>$status_code,
                'message' => $message,
                'data' => $data,
            ],
            $status_code
        );
	}

}


if (! function_exists('respondWithXML')) {
    function respondWithXML($success,$status_code=200, $message=[],$data=[])
	{
		 
          return response()->xml([
                'success' => (string) $success,
                'status'=>$status_code,
                'message' => $message,
                'data' => $data,
            ]);
        
	}

}


if (! function_exists('parseToXML')) {
    function parseToXML($data=[])
	{
        $collection=collect($data);

        return $collection->toXml();

        
	}

}

