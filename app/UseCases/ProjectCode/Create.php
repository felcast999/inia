<?php

namespace App\UseCases\ProjectCode;

use App\Models\CodigoProyecto;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Carbon;

class Create
{
    private $model;


    public function __construct(
        CodigoProyecto $model

    )
    {
        $this->model = $model;

    }

    public function execute(Request $request)
    {
  

       $tiempo_creacion="{$request->tiempo_creacion['year']}-{$request->tiempo_creacion['month']}-{$request->tiempo_creacion['day']}";

       $project_code = $this->model->create([
        "tiempo_creacion"=>$tiempo_creacion,
        "actividades_idi_id"=>$request->actividades_idi_id,
        "dependencia_id"=>$request->dependencia_id ,
        "sede_id"=>$request->sede_id ,
        "numero_orden"=>$request->numero_orden,
        "ano"=>$request->ano
    ]);
   

        return $project_code;
    }


   
}