<?php

namespace App\UseCases\ProjectCode;

use App\Models\CodigoProyecto;

class Show
{
    private $model;

    public function __construct(
        CodigoProyecto $model
    )
    {
        $this->model = $model;
    }

    public function execute($id)
    {
        $project_code = $this->model->findOrFail($id);

        $project_code->load('actividad');
        $project_code->load('dependencia');
        $project_code->load('sede');

        return compact('project_code');
    }
}