<?php

namespace App\UseCases\ProjectCode;

use App\Models\CodigoProyecto;

class Destroy
{
    private $model;

    public function __construct(
        CodigoProyecto $model
    )
    {
        $this->model = $model;
    }

    public function execute($id)
    {
        return  $this->model->destroy($id);
    }
}