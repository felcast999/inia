<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    use HasFactory;

     protected $fillable = [
    	'name',
    	'status_type_id'
    ];


	 public function status_type()
    {
        return $this->belongsTo(StatusType::class,'status_type_id','id');
    }

}
