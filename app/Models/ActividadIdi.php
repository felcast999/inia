<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ActividadIdi extends Model
{
    use HasFactory;

    protected $table="actividades_idi";

}
