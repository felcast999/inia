<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CodigoProyecto extends Model
{
    use HasFactory;

    protected $table="codigo_proyecto";

    protected $fillable = [
        'numero_orden',
        'dependencia_id',
        'sede_id',
        'actividades_idi_id',
        'ano',
        'tiempo_creacion',
    ];

    //getters
    public function getNumeroOrdenAttribute()
    {
        return str_pad($this->attributes['numero_orden'], 4, '0', STR_PAD_LEFT);
    }

    public function actividad()
    {

        return $this->belongsTo(ActividadIdi::class,'actividades_idi_id','id');

    }

    
    public function dependencia()
    {

        return $this->belongsTo(Dependencia::class,'dependencia_id','id');

    }

    public function sede()
    {

        return $this->belongsTo(Sede::class,'sede_id','id');

    }
}
