<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;

    protected $table="clients";

    protected $fillable=[
                        "first_name","last_name","identification_card_number","identification_card_type_id","direction","email","phone","local_phone","business_name","status_id"
                        ];

    protected $appends = ['full_name'];

    public function identification_card_type()
    {
        return $this->belongsTo(IdentificationCardType::class,'identification_card_type_id');
    }

  public function getFullNameAttribute()
    {
    
        return $this->attributes['first_name'].' '.$this->attributes['last_name'];
    }


}





