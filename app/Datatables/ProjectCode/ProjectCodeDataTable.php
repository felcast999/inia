<?php

namespace App\Datatables\ProjectCode;

use App\Datatables\BaseDataTable;
use App\Models\CodigoProyecto;
use Yajra\DataTables\EloquentDataTable;

class ProjectCodeDataTable extends BaseDataTable
{
	public function __construct(CodigoProyecto $model)
	{
		parent::__construct($model);
	}

	protected function makeQuery():void
	{	
		$query = $this->model
						->query()
						->join('actividades_idi','actividades_idi.id','=','codigo_proyecto.actividades_idi_id')
						->join('dependencia','dependencia.id','=','codigo_proyecto.dependencia_id')
						->join('sede','sede.id','=','codigo_proyecto.sede_id')
						->select('codigo_proyecto.*','actividades_idi.sigla as actividades_idi','dependencia.sigla as dependencia_sigla','sede.sigla as sede_sigla'); 

		$this->setQuery($query);
	}

	protected function makeColumns():EloquentDataTable
	{
		return $this->new_dataTable
					->filterColumn('actividades_idi', function($query, $keyword) {

						$word=str_replace(' ','%',$keyword);

						$sql = "actividades_idi.sigla like ?";
						$query->whereRaw($sql, ["%{$word}%"]);
					})
					->filterColumn('dependencia_sigla', function($query, $keyword) {

						$word=str_replace(' ','%',$keyword);

						$sql = "dependencia.sigla like ?";
						$query->whereRaw($sql, ["%{$word}%"]);
					})
					->filterColumn('sede_sigla', function($query, $keyword) {

						$word=str_replace(' ','%',$keyword);

						$sql = "sede.sigla like ?";
						$query->whereRaw($sql, ["%{$word}%"]);
					})
					->addColumn('_sigla_actividad', function($model){

						return $model->actividad->sigla;

                    })
					->addColumn('_sigla_dependencia', function($model){

						return $model->dependencia->sigla;

                    })
                    ->addColumn('_sigla_sede', function($model){

						return $model->sede->sigla;

                    })
					->addColumn('_codigo_completo', function($model){

						return "{$model->actividad->sigla}-{$model->dependencia->sigla}-{$model->sede->sigla}-{$model->numero_orden}-{$model->ano}";

                    })
					->addColumn('_actions', function($model){

						return view('dataTables.project-code.actions-column', compact('model'));

					})
					->rawColumns([
						'_sigla_actividad',
						'_sigla_dependencia',
						'_sigla_sede',
						'_codigo_completo',
						'_actions'
					]);
	}

}