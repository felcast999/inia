<?php

use Illuminate\Support\Facades\Route;

Route::group([
	'prefix' => 'resources',
	'namespace' => 'resources'
], function(){


	Route::group([
		'prefix' => 'project-codes'
	],
	 function(){
	
	
		//identification card types
		Route::get('/get-codes', 'ProjectCodeController@get');
	
	});


});
