<p>
<h4> Pasos para installar</h4>
<ul>
<li>Clonar el repositorio</li>
<li>Importar la base de datos</li>
<li>Ejecutar el comando <pre>composer install</pre></li>
<li>Ejecutar el comando <pre>php artisan passport:install</pre></li>
<li>Ejecutar el comando <pre>php artisan key:generate</pre></li>
<li>Dentro de la carpeta <pre>resources/coreui</pre> ejecutar el comando <pre>npm install</pre></li>
<li>Registrarse</li>

</ul>

<p><a href="https://gitlab.com/felcast999">Felipe Castillo</a></p>
</p>