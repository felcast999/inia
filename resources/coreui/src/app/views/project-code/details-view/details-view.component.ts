import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { formatDate } from '../../../shared/helpers';
import {environment} from '../../../../environments/environment';

@Component({
  templateUrl: './details-view.component.html',
  styleUrls: ['./details-view.component.scss']
})
export class DetailsViewComponent implements OnInit {

	public project_code;
  public code:string;
  public baseUrl:string;


  constructor(
  	private route:ActivatedRoute
  ) { 

    this.baseUrl = environment.host;

  }

  ngOnInit()
  {
  	this.route.data.subscribe((data:any) =>{ 
      this.project_code = data.model.project_code
      this.code=`${this.project_code.actividad.sigla}-${this.project_code.dependencia.sigla}-${this.project_code.sede.sigla}-${this.project_code.numero_orden}-${this.project_code.ano}`;
    });
  }

  public formatDate(date:string|Date, format?:string):string
  {
  		return formatDate(date,format);
  }
}  
