import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProjectCodeComponent } from './project-code.component';
import { DetailsViewComponent } from './details-view/details-view.component';

// resolvers
import { ShowResolverService } from '../../services/solvers/project-code/show-resolver.service';


const routes: Routes = [{
  path: '',
  data: {
    title: 'Codigo de proyectos'
  },
  children: [
    
    {
      path: '',
      component: ProjectCodeComponent,
      data: {
        title: "Codigo de proyectos"
      }
    },
    {
      path: ':id',
      component: DetailsViewComponent,
      data: {
        title: "Detalles",
        animationState: 'project-codes' 
      },
      resolve: {
        model: ShowResolverService
      }
    }
  ] 
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    ShowResolverService
  ]
})
export class ProjectCodeRoutingModule { }
