import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { ProjectCodeRoutingModule } from './project-code-routing.module';

import { ProjectCodeComponent } from './project-code.component';
import { TableComponent } from './table/table.component';
import { CreationModalComponent } from './creation-modal/creation-modal.component';
import { DetailsViewComponent } from './details-view/details-view.component';

import { ProjectCodeService } from '../../services/project-code.service';
import { ResourceService } from '../../services/resource.service';

import {  AuthInterceptor } from '../../services/http-interceptors/auth-interceptor.service';


import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';

import { DataTablesModule } from 'angular-datatables';
import { HttpClient, HttpHeaders, HttpClientModule,HTTP_INTERCEPTORS } from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {BrowserModule} from '@angular/platform-browser';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    ProjectCodeComponent,
    CreationModalComponent,
    TableComponent,
    DetailsViewComponent,
    ],
  imports: [
    ProjectCodeRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    DataTablesModule,
    NgSelectModule,
    NgbModule
  ],
  providers: [
    ProjectCodeService,
    ResourceService,
    { 
   provide: HTTP_INTERCEPTORS,
   useClass: AuthInterceptor, 
   multi: true 
    }
  ]
})
export class ProjectCodeModule { }
