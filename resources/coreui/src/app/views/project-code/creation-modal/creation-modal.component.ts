import { Component, Input, Output, ViewChild, EventEmitter, OnInit,ElementRef } from '@angular/core';
import {ModalDirective} from 'ngx-bootstrap/modal';
import {NgForm,FormControl,FormGroup,Validators,FormBuilder,FormArray } from '@angular/forms';

// services
import swal from 'sweetalert2';
import { ProjectCodeService as CRUDService } from '../../../services/project-code.service';

import { showPreconfirmMessage } from '../../../shared/helpers';
import { Observable } from 'rxjs';

import { ProjectCodeForm as FormModel } from '../../../form-models/project-code-form';
import {environment} from '../../../../environments/environment';
import {NgbDateStruct, NgbCalendar} from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'creation-modal',
  templateUrl: './creation-modal.component.html',
  styleUrls: ['./creation-modal.component.css'],
  providers: [
  ]
})
export class CreationModalComponent implements OnInit {

  @ViewChild('modalCR')
  public modal: ModalDirective;
  


  @ViewChild('formCR')
  public form:NgForm;
  
  @Output()
  public reloadTable: EventEmitter<any> = new EventEmitter();

  public loadingBatches:boolean;

  
  public sendingForm:boolean;
  
  public actividades_idi_ids:any;
  
  public dependencia_ids:any;
  public sede_ids:any;

  public form_model:any;

  public model_form_control:any;
  
  public baseUrl:string;
 
  public tiempo_creacion: NgbDateStruct;
  public date: {year: number};
  public model: NgbDateStruct;
  public anos:Array<string>=[];

  constructor(
    private _crudService: CRUDService,
    private fb: FormBuilder,
    private calendar: NgbCalendar
   )
  {
    this.baseUrl = environment.host;


    this.form_model= new FormModel(this.fb);

    //this.dateTimeAdapter.setLocale('es-ES'); 



    this.model_form_control= this.form_model.model_form_control;

    this.sendingForm = false;

    this.loadingBatches = false;

    this.anos =this.generate_years(2019-20);
   
  }

  ngOnInit():void
  {

  }

  private generate_years(startYear) {
    let currentYear = new Date().getFullYear(), years = [];
    startYear = startYear || 1980;  
    while ( startYear <= currentYear ) {
        years.push(startYear++);
    }   
    return years;
}

 
  public async showPreconfirmMessage():Promise<void>
  {

  let message:string='';

  switch(this.model_form_control._method.value) { 
   case 'POST': { 

      message='Añadir Codigo de Proyecto?';

      break; 
   } 
   case 'PUT': { 
      message='Modificar Codigo de Proyecto?';

      break; 
   } 
  
   } 


  const response = await showPreconfirmMessage(
                            message,
                            "",
                            "info",
                            "Cancelar",
                            "Si"
                            );

    if (response.value)
      this.sendForm();
  }

  private sendForm():void
  {
  	if (this.sendingForm)
    {
  		return;

    }

    this.sendingForm = true;


   switch(this.form_model.model_form_control._method.value) { 
   case 'POST': { 

         this._crudService.store(this.form_model.form.value).subscribe(
      response => this.handleResponse(response),
      err =>{
        console.error('Observer got an error: ' + err)
            this.sendingForm = false

    },
      () => {
    this.sendingForm = false

      }
     );

      break; 
   } 
   case 'PUT': { 

    console.log(this.form_model.form.value)
         this._crudService.update(this.form_model.form.value).subscribe(
      response => this.handleResponse(response),
      err =>{
        console.error('Observer got an error: ' + err)
            this.sendingForm = false

    },
      () => {
    this.sendingForm = false

      }
     );
      break; 
   } 
  
   } 



   



  }


  

  private handleResponse(response):void
  {
    const action = () => {
      this.reloadTable.emit(true);
      this.modal.hide();
    };

    swal.fire('Exito!',response.message,'success')
        .then( () =>  action())
        .catch( () => action());
  }

  public clearForm():void
  {
    this.form_model.form.reset();

    this.sendingForm = false;
   


  }

}
