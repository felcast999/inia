import { Component, AfterViewInit, ViewChild } from '@angular/core';
import { TableComponent } from './table/table.component';
import { CreationModalComponent } from './creation-modal/creation-modal.component';

import { formatDate, now, showPreconfirmMessage } from '../../shared/helpers';
import { ProjectCodeService as CRUDService } from '../../services/project-code.service';

import { NgForm, FormControl, FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';

import { ToastrService } from 'ngx-toastr';
import { ResourceService } from '../../services/resource.service';
import { DatePipe } from '@angular/common';


@Component({
  templateUrl: './project-code.component.html',
  styleUrls: ['./project-code.component.scss']
})
export class ProjectCodeComponent implements AfterViewInit {



  @ViewChild(CreationModalComponent)
  public creattionModal: CreationModalComponent;




  @ViewChild(TableComponent)
  public table: TableComponent;

  constructor(
    private _crudService: CRUDService,
    private toast: ToastrService,
    private fb: FormBuilder,
    private _resourceService: ResourceService,
    private datePipe: DatePipe

  ) { }

  ngAfterViewInit(): void {



    this.table.init();
  }

  public reloadTable(): void {
    this.table.reload();
  }





  public showCreationModal(event?: any): void {



    if (typeof event !== 'undefined') {

      //getting user
      this._crudService.show(event.id).subscribe(data => {
        let date=new Date(this.datePipe.transform(data.project_code.tiempo_creacion,"yyyy-MM-dd"));
        let year=date.getFullYear();
        let month=date.getMonth();
        let day=date.getDate();


        this.creattionModal.form_model.form.patchValue({
          _method: 'PUT',
          actividades_idi_id: data.project_code.actividades_idi_id,
          dependencia_id: data.project_code.dependencia_id,
          sede_id: data.project_code.sede_id,
          ano:data.project_code.ano,
          tiempo_creacion:{ year: year, month: month, day: day },
          project_code_id: event.id,
          numero_orden:data.project_code.numero_orden
        });


        this.creattionModal.modal.show();

      })


    } else {

      //getting 
      this._crudService.get_project_codes().subscribe(data => {


        this.creattionModal.actividades_idi_ids = data.actividades_idi_ids;
        this.creattionModal.dependencia_ids = data.dependencia_ids;
        this.creattionModal.sede_ids = data.sede_ids;
        this.creattionModal.form_model.form.controls['numero_orden'].setValue(data.order_number); 

        this.creattionModal.form_model.form.controls['_method'].setValue('POST');
        this.creattionModal.modal.show();

      })



    }




  }



  public delete(event: any): void {
    showPreconfirmMessage(
      "¿Eliminar Codigo de Proyecto?",
      ""
    )
      .then(result => {

        if (result.value) {
          this._crudService.delete(event.id).subscribe(
            response => this.handleResponseOfRequest(response));
        }

      });
  }




  private handleResponseOfRequest(response: any): void {
    this.toast.success(response.message, 'Exito!');
    this.reloadTable();
  }

}
