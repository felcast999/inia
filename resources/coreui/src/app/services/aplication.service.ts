import { Injectable } from '@angular/core';
import {HttpClient,HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import { map } from 'rxjs/operators';
import { Aplication } from '../models/aplication';

@Injectable()
export class AplicationService {

	private baseUrl:string;

	constructor(
		private _http: HttpClient
		)
	{
		this.baseUrl = environment.APIEndpoint;
	}


	setAuthenticationHeaders(): object
	{
		const token = localStorage.getItem('token'),
		httpHeaders = {
			headers: new HttpHeaders({
				'Authorization': `Bearer  ${token}`
			})
		};

		return httpHeaders;
	}

	public delete(id:number):Observable<any>
	{
		return this._http.delete(`${this.baseUrl}/aplications/${id}`);
	}


    public getDataTable(dataTablesParameters: object): Observable<any>
	{
		return this._http.post(`${this.baseUrl}/datatables/aplications`, dataTablesParameters);
	}

	public store(data:any):Observable<any>
	{
		//const httpHeaders = this.setAuthenticationHeaders();

		return this._http.post(`${this.baseUrl}/aplications`, data);
	}


	public update(data:any):Observable<any>
	{
		//const httpHeaders = this.setAuthenticationHeaders();

		return this._http.post(`${this.baseUrl}/aplications/${data.product_id}`, data);
	}


	public show(id:number):Observable<any>
	{
    return this._http
              .get(`${this.baseUrl}/aplications/${id}`);
	}
	

}

