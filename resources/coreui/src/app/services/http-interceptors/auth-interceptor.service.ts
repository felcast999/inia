import { Injectable } from '@angular/core';
import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest
} from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { TokenService } from '../auth/token.service';
import { ErrorResponseService } from '../auth/error-response.service';

import {map, catchError, tap} from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(
  	private _tokenService: TokenService,
    private _errorResponseService: ErrorResponseService
  	){}

  intercept(req: HttpRequest<any>, next: HttpHandler)
  {
    const authReq = req.clone({ 
      setHeaders:{'Authorization':`Bearer ${localStorage.getItem('token')}`}
    });
      
    return next.handle(authReq)
              .pipe(
                  // tap(response => response),
                  catchError(error => {

                    this._errorResponseService.handle(error);

                    return throwError(error);
                  })
                );
  }
  
}