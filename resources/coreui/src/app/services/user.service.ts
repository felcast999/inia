import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { environment } from '../../environments/environment';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class UserService {
	public url: string;
	public identity;
	public token;

	private httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

	constructor(
		public _http: HttpClient
	){
		this.url = environment.APIEndpoint;
	}


	disable(data)
	{
      return this._http.post<any>(environment.APIEndpoint+`/config/disable-user`,data,this.httpOptions)

	}


	store(data,url)
  	{
      return this._http.post<any>(environment.APIEndpoint+`/`+url,data,this.httpOptions)

  	}


  		update(data,url)
  	{
      return this._http.post<any>(environment.APIEndpoint+`/`+url,data,this.httpOptions)

  	}


	find(id)
	{
		   return this._http
          .get(
            this.url+'/config/users/'+id,{}
          )
	}

	getcards()
	{
		   return this._http
          .get(
            this.url+'/get-cards',{}
          )
	}

	getRoles(id)
	{
		   return this._http
          .post<any>(
            this.url+'/config/get-all-roles',{user_id:id},this.httpOptions
          )
	}

   
 	delete(id)
  	{
      return this._http.delete<any>(environment.APIEndpoint+`/config/users/`+id,this.httpOptions)

  	}




}
