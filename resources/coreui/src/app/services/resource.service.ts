import { Injectable } from '@angular/core';
import {HttpClient,HttpHeaders} from '@angular/common/http';
import {environment,version} from '../../environments/environment';
import {Observable} from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()

export class ResourceService {

  private baseUrl:string;

	constructor(
		private _http: HttpClient
		)
	{
		this.baseUrl = environment.host;
	}


	setAuthenticationHeaders(): object
	{
		const token = localStorage.getItem('token'),
		httpHeaders = {
			headers: new HttpHeaders({
				'Authorization': `Bearer  ${token}`
			})
		};

		return httpHeaders;
	}


  public get_identification_card_types():Observable<any>
  {
		return this._http.get(`${this.baseUrl}/resources/get-card-types`);

  }

}
