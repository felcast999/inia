import { Injectable } from '@angular/core';
import {
	Resolve,
	ActivatedRouteSnapshot,
	RouterStateSnapshot
} from '@angular/router';
import {Observable, of} from 'rxjs';

import { WorkerService } from '../../worker.service';
import { Worker } from '../../../models/worker';
import { map,catchError } from 'rxjs/operators';

@Injectable()
export class ShowResolverService implements Resolve<any>
{
  constructor(
  	private _model:WorkerService
  ) { }
  
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Worker|any>
  {
      const id = Number(route.paramMap.get('id'));

  		return this._model
  					   .show(id);
  }





}

