import { Injectable } from '@angular/core';
import {
	Resolve,
	ActivatedRouteSnapshot,
	RouterStateSnapshot
} from '@angular/router';
import {Observable, of} from 'rxjs';

import { ProjectCodeService } from '../../project-code.service';
import { ProjectCode } from '../../../models/project-code';
import { map,catchError } from 'rxjs/operators';

@Injectable()
export class ShowResolverService implements Resolve<any>
{
  constructor(
  	private _projectCodeService:ProjectCodeService
  ) { }
  
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ProjectCode|any>
  {
      const id = Number(route.paramMap.get('id'));

  		return this._projectCodeService
  					   .show(id);
  }





}

