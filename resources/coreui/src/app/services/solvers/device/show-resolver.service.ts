import { Injectable } from '@angular/core';
import {
	Resolve,
	ActivatedRouteSnapshot,
	RouterStateSnapshot
} from '@angular/router';
import {Observable, of} from 'rxjs';

import { DeviceService } from '../../device.service';
import { Device } from '../../../models/device';
import { map,catchError } from 'rxjs/operators';

@Injectable()
export class ShowResolverService implements Resolve<any>
{
  constructor(
  	private _model:DeviceService
  ) { }
  
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Device|any>
  {
      const id = Number(route.paramMap.get('id'));

  		return this._model
  					   .show(id);
  }





}

