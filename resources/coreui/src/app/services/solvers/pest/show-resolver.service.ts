import { Injectable } from '@angular/core';
import {
	Resolve,
	ActivatedRouteSnapshot,
	RouterStateSnapshot
} from '@angular/router';
import {Observable, of} from 'rxjs';

import { PestService } from '../../pest.service';
import { Pest } from '../../../models/pest';
import { map,catchError } from 'rxjs/operators';

@Injectable()
export class ShowResolverService implements Resolve<any>
{
  constructor(
  	private _model:PestService
  ) { }
  
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Pest|any>
  {
      const id = Number(route.paramMap.get('id'));

  		return this._model
  					   .show(id);
  }





}

