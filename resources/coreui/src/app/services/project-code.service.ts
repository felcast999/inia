import { Injectable } from '@angular/core';
import {HttpClient,HttpHeaders} from '@angular/common/http';
import {environment,version} from '../../environments/environment';
import {Observable} from 'rxjs';
import { map } from 'rxjs/operators';
import { ProjectCode } from '../models/project-code';

@Injectable()
export class ProjectCodeService {

	private baseUrl:string;

	constructor(
		private _http: HttpClient
		)
	{
		this.baseUrl = environment.host;
	}


	setAuthenticationHeaders(): object
	{
		const token = localStorage.getItem('token'),
		httpHeaders = {
			headers: new HttpHeaders({
				'Authorization': `Bearer  ${token}`
			})
		};

		return httpHeaders;
	}

	public delete(id:number):Observable<any>
	{
		return this._http.delete(`${this.baseUrl}/${version}/project-codes/${id}`);
	}

	public get_project_codes():Observable<any>
	{
		return this._http.get(`${this.baseUrl}/resources/project-codes/get-codes`);
	
	}

    public getDataTable(dataTablesParameters: object): Observable<any>
	{
		return this._http.post(`${this.baseUrl}/datatables/project-codes`, dataTablesParameters);
	}

	public store(data:any):Observable<any>
	{

		return this._http.post(`${this.baseUrl}/${version}/project-codes`, data);
	}



	public update(data:any):Observable<any>
	{

		return this._http.post(`${this.baseUrl}/${version}/project-codes/${data.project_code_id}`, data);
	}


	public show(id:number):Observable<any>
	{
    return this._http
              .get(`${this.baseUrl}/${version}/project-codes/${id}`);
	}
	




}

