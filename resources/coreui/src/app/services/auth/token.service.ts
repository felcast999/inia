import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import jwt_decode from "jwt-decode";
import * as moment from 'moment';

@Injectable()

export class TokenService {

  private iss: object;

  constructor()
  {
  	this.iss = {
  		login: `${environment.host}/login`,
  		register: `${environment.host}/register`,
  	};
  }

  handle(token)
  {
  	this.setToken(token);
  }

  setToken(token) :void
  {
  	localStorage.setItem('token',token);
  }

  getToken() :string
  {
  	 return localStorage.getItem('token');
  }

  removeToken() :void
  {
  	 localStorage.removeItem('token');
  }

  isValid()
  {
  	const token = this.getToken();

    let decode=(token!=undefined)?jwt_decode(token):'';
    let exp_date = new Date(decode["exp"]*1000);
    let current_date=new Date();
    let isAfter = moment(current_date).isAfter(exp_date);


  	if (!isAfter)
  	{
      if(decode!="")
      {
        return true;

      }
  
  	}

    return false;

  }

 

  loggedIn()
  {
  	return this.isValid();
  }
}
