import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from '../../models/user';

@Injectable({
  providedIn: 'root'
})

export class AuthUserService {

  private authenticatedUser$: BehaviorSubject<User>;

  constructor(
  )  
  {
    this.authenticatedUser$ = new BehaviorSubject<User>(null); 
  }

  public updateAuthUser(user:User): void
  {
    this.authenticatedUser$.next(user);
  }

  public getAuthUser() : Observable<User>
  {
    return this.authenticatedUser$.asObservable();
  }

  public updateAuthUserData(data:any) :void
  {
    const authUser = User.createInstance(data);
    this.updateAuthUser(authUser);
  }

  public removeAuthUser(): void
  {
    this.updateAuthUser(null);
  }

}
