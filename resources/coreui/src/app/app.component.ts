import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd,RouterOutlet } from '@angular/router';
import { IconSetService } from '@coreui/icons-angular';
import { cilUser } from '@coreui/icons';
import { routeTransitionAnimations } from './route-transition-animations';

@Component({
  // tslint:disable-next-line
  selector: 'body',
  templateUrl: './app.component.html',
  providers: [IconSetService],
  animations: [routeTransitionAnimations]

})
export class AppComponent implements OnInit {
  constructor(
    private router: Router,
    public iconSet: IconSetService
  ) {
    // iconSet singleton
    iconSet.icons = { cilUser };
  }

  ngOnInit() {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0);
    });
  }


  prepareRoute(outlet: RouterOutlet) {
   
      
    return outlet && 
      outlet.activatedRouteData && 
      outlet.activatedRouteData['animationState'];
   }
 
}
