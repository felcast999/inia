import {Component} from '@angular/core';
import { navItems } from '../../_nav';
import { JwtService } from '../../services/auth/jwt.service';
import { Router,RouterOutlet } from '@angular/router';
import { routeTransitionAnimations } from '../../route-transition-animations';

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html',
  animations: [routeTransitionAnimations]

})


export class DefaultLayoutComponent {

  constructor(
    private _jwtService: JwtService,
    private _router:Router,
  	)
  {
  
  }

  public sidebarMinimized = false;
  public navItems = navItems;

  toggleMinimize(e) {
    this.sidebarMinimized = e;
  }

  prepareRoute(outlet: RouterOutlet) {
   
      
    return outlet && 
      outlet.activatedRouteData && 
      outlet.activatedRouteData['animationState'];
   }

  logout()
  {
    this._jwtService.logout().subscribe(
			response => {
        this._router.navigateByUrl('/login')

			},
			error => console.log(error)
		);
  }
}
