import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Import Containers
import { DefaultLayoutComponent } from './containers';

import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { LoginComponent } from './views/login/login.component';
import { RegisterComponent } from './views/register/register.component';


import { RequestResetPasswordComponent } from './views/request-reset-password/request-reset-password.component';
import { ResponsePasswordResetComponent } from './views/response-password-reset/response-password-reset.component';

import {BeforeLoginService} from './services/auth/before-login.service';
import {AfterLoginService} from './services/auth/after-login.service';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: '404',
    component: P404Component,
    data: {
      title: 'Page 404'
    }
  },
  {
    path: '500',
    component: P500Component,
    data: {
      title: 'Page 500'
    }
  },
  {
    path: 'login',
    component: LoginComponent,
    canActivate:[BeforeLoginService],
    data: {
      title: 'Login Page',
      animationState: 'login' 
    }
  },
  {
    path: 'register',
    component: RegisterComponent,
    canActivate:[BeforeLoginService],
    data: {
      title: 'Register Page',
      animationState: 'register' 

    }
  },
  {
  path: 'request-reset-password',
  component: RequestResetPasswordComponent,
  canActivate:[BeforeLoginService]
 },
 {
  path: 'response-password-reset',
  component: ResponsePasswordResetComponent,
  canActivate:[BeforeLoginService]
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    canActivate:[AfterLoginService],
    data: {
      title: 'Home'

    },
    children: [
      {
        path: 'project-codes',
        loadChildren: () => import('./views/project-code/project-code.module').then(m => m.ProjectCodeModule),
         canActivate:[AfterLoginService],
         data: {
          allowed_roles: ['all'],
          animationState: 'project-codes' 

        }
 
      },
      {
        path: 'base',
        loadChildren: () => import('./views/base/base.module').then(m => m.BaseModule)
      },
      {
        path: 'buttons',
        loadChildren: () => import('./views/buttons/buttons.module').then(m => m.ButtonsModule)
      },
      {
        path: 'charts',
        loadChildren: () => import('./views/chartjs/chartjs.module').then(m => m.ChartJSModule)
      },
      {
        path: 'dashboard',
        loadChildren: () => import('./views/dashboard/dashboard.module').then(m => m.DashboardModule),
        data:{
         animationState: 'dashboard' 
        }
      },
      {
        path: 'icons',
        loadChildren: () => import('./views/icons/icons.module').then(m => m.IconsModule)
      },
      {
        path: 'notifications',
        loadChildren: () => import('./views/notifications/notifications.module').then(m => m.NotificationsModule)
      },
      {
        path: 'theme',
        loadChildren: () => import('./views/theme/theme.module').then(m => m.ThemeModule)
      },
      {
        path: 'widgets',
        loadChildren: () => import('./views/widgets/widgets.module').then(m => m.WidgetsModule)
      }
    ]
  },
  { path: '**', component: P404Component }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' }) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
