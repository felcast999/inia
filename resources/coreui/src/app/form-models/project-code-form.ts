import {NgForm,FormControl,FormGroup,Validators,FormBuilder,FormArray } from '@angular/forms';


export class ProjectCodeForm {
 
 public form:FormGroup;



constructor(
		    private fb?: FormBuilder
	)
	{

     this.form = this.fb.group({
      _method:new FormControl('POST'),
      actividades_idi_id :new FormControl('',Validators.required),
      dependencia_id :new FormControl('',Validators.required),
      sede_id : new FormControl('',Validators.required),
      numero_orden: new FormControl('',Validators.required),
      ano: new FormControl('',Validators.required),
      tiempo_creacion: new FormControl('',Validators.required),
      project_code_id:null

    });

	}



 get model_form_control() {
    return this.form.controls;
  }



  get is_invalid() {
    return this.form.invalid;
  }
  


}
