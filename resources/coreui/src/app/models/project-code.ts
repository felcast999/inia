export class ProjectCode {
   
constructor(
		public id:number,
		public actividades_idi_id :number,
		public dependencia_id :number,
		public sede_id :number,
		public numero_orden:string,
		public ano:number,
		public tiempo_creacion:Date,
		public created_at?:string,
		public updated_at?:string,
	)
	{

	}


/* Methods */

	static createInstance(data:any)
	{
		const {id,actividades_idi_id,dependencia_id,sede_id,numero_orden,ano,tiempo_creacion,created_at,updated_at} = data;
		return new ProjectCode(id,actividades_idi_id,dependencia_id,sede_id,numero_orden,ano,tiempo_creacion,created_at,updated_at);   
	}


}
